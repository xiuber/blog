---
title: 初识solidity
categories: 区块链
tags: solidity
date: 2021-03-04 11:00:00
---

1.  通过代码学习 solidity

```javascript
// SPDX-License-Identifier: GPL-3.0  //源代码版权许可
pragma solidity >=0.4.16 <0.9.0;//告诉编译器源代码solidity版本>=0.4.16,<0.8.0
contract SimpleStorage {//声明合约名称
    uint storedData;// 声明一个类型为uint（256位无符号整数）的变量，名字叫做  storageData

    function set(uint x) public {// 函数 set ，设置变量的值
        storedData = x;
    }

    function get() public view returns (uint) {// 函数get，取出变量的值。
        return storedData;
    }
}
```

2.  一个简单的货币合约

```javascript
// SPDX-License-Identifier: GPL-3.0 //源代码版权许可
pragma solidity  >=0.7.0 <0.9.0; //告诉编译器源代码solidity版本>=0.7.0,<0.9.0

contract Coin {//声明合约主体
    address public minter;// 声明了一个可以公开访问的 address类型的变量（一个160位的值，，且不可以任何算数操作）。public 让这个变量可以从外部读取。如果没有这个关键字其他合约就无法访问这个变量。
    mapping (address => uint) public balances;// 声明了一个公共变量 mapping 更为复杂的数据类型。该类型将address映射成为uint无符号整数。

    event Sent(address from, address to, uint amount);//声明了一个event事件，它会在Send函数最后一行发出。可以被监听该事件的所有用户监听到，同时包含了三个参数，address from，address to，amount。假如我们现在可以用web3.js 去监听 ，代码如：3.

    constructor() { // 构造函数 ，创建合约时候才运行，他会永久的创建合约人地址msg，全局变量，包含一些允许访问区块链的属性。msg.sender 始终是 外部函数调用的来源地址。
        minter = msg.sender;
    }

    function mint(address receiver, uint amount) public {// mint 函数，被其他合约调用的函数，
        require(msg.sender == minter);//只有sender 是合约创建者时候调用继续执行，相当于合约创建者之外的人调用什么也不会发生。
        require(amount < 1e60);//只有amount 小于 1e60的时候才会继续执行。
        balances[receiver] += amount;// 上面俩条件 全部符合 接受者才会收到amount的Coin。
    }

    function send(address receiver, uint amount) public {// send 函数，
        require(amount <= balances[msg.sender], "Insufficient balance.");// 要求发送者sender拥有的Coin数量要大于将要发送给别人的amount数量，才会继续执行。否则提示：‘Insufficient balance.’
        balances[msg.sender] -= amount;//发送者sender 减少amount数量的Coin
        balances[receiver] += amount;// 接受者 增加amount数量的Coin
        emit Sent(msg.sender, receiver, amount);// 最后调用Sent函数，可以被监听的人收到。
    }
}
```

3.  web3.js 监听 Send 函数，假设 Coin 已经被创建成为合约对象。（以上简单的货币合约，我们将在学习完 web3.js 后，进行本地实际虚拟链上操作，后期直接应用 将不再复述代码含义。）

```javascript
Coin.Sent().wacth({}, "", function (error, res) {
  if (!error) {
    console.log('Coin transfer':+res.args.amount+'Coin were sent from'+result.args.from+'Coin were to'+result.args.to);
  }else{
        "Balances now:\n" +
        "Sender: " + Coin.balances.call(result.args.from) +
        "Receiver: " + Coin.balances.call(result.args.to)
  }
});
```

4.  投票合约

```javascript
// SPDX-License-Identifier: GPL-3.0 // 声明源代码许可
pragma solidity >=0.7.0 <0.9.0;// 声明编译器版本
contract Ballot { //声明了一个复合类型   选票
    struct Voter {// 声明了个选民，代表是个选民
        uint weight;//声明了一个计票权重
        bool voted;// 布尔型，若为真，代表选民已经投票
        address delegate;// 被委托人
        uint vote;// 投票提案的索引
    }
    struct Proposal {// 代表一个提案，
        bytes32 name;//提案简称
        uint voteCount;//得票数
    }
    address public chairperson;// 定义了一个公开访问的address状态变量。
    mapping(address => Voter) public voters;// 为每个address存储一个选民voter
    Proposal[] public proposals;// 声明一个Proposal类型的数组，里面是提案的集合

    constructor(bytes32[] memory proposalNames) {// 为proposalNames中的每个提案 创建一个投票表决。
        chairperson = msg.sender;
        voters[chairperson].weight = 1;//权重为1
        for (uint i = 0; i < proposalNames.length; i++) {
            //对于提供的每个提案的名称，都创建一个新的Proposal对象，并且把它们push到proposals末尾。
            proposals.push(Proposal({
                name: proposalNames[i],
                voteCount: 0
            }));
        }
    }

    function giveRightToVote(address voter) public {// 授权选民对这个表决进行投票。
        require(
            msg.sender == chairperson,//sender 必须是chairperson，才可以继续执行。
            "Only chairperson can give right to vote."
        );
        require(
            !voters[voter].voted,//如果该选民已经投过票了 ，将不再继续执行，返回消息‘The voter already voted’
            "The voter already voted."
        );
        require(voters[voter].weight == 0);// 这个选民权重是0 才可以继续
        voters[voter].weight = 1;// 权重+1
        //require :require的第一个参数结果为true才会继续执行，为false，将不再继续执行。第二个参数为错误消息。
    }

    function delegate(address to) public {//该函数，把你的投票委托给（传递给）投票者。to
        Voter storage sender = voters[msg.sender];
        require(!sender.voted, "You already voted.");//投过票了就不再执行。
        require(to != msg.sender, "Self-delegation is disallowed.");//不能在委托给自己。
        while (voters[to].delegate != address(0)) {
            to = voters[to].delegate;
            require(to != msg.sender, "Found loop in delegation.");//不能够闭环委托，我委托了张三，张三给了李四，李四有给了我。这种闭环不可以发生。
        }
        sender.voted = true;// sender是引用，相当于对 voters[msg.sender].voted=true,修改sender的投票状态。
        sender.delegate = to;
        Voter storage delegate_ = voters[to];//delegate_ 被委托的人
        if (delegate_.voted) {// 如果被委托者，已经投票了。
            proposals[delegate_.vote].voteCount += sender.weight;// 那么将获得票数。
        } else {// 如果没有
            delegate_.weight += sender.weight;// 则增加委托者的权重
        }
    }
    function vote(uint proposal) public {
        Voter storage sender = voters[msg.sender];//定义sender引用
        require(!sender.voted, "Already voted.");
        sender.voted = true;//相当于 voters[msg.sender].voted = true;修改投票状态为已经投票
        sender.vote = proposal;//相当于 voters[msg.sender].vote = proposal;把投票的索引放入proposal中
        proposals[proposal].voteCount += sender.weight;//次提案得票数+=sender的权重，如果proposals 超过数组范围，则抛出异常，恢复所有改动。
    }
    function winningProposal() public view
            returns (uint winningProposal_)
    {//整理投票，计算出得票最多的提案。
        uint winningVoteCount = 0;
        for (uint p = 0; p < proposals.length; p++) {//定义一个p 取proposals中voteCount最大者、
            if (proposals[p].voteCount > winningVoteCount) {
                winningVoteCount = proposals[p].voteCount;
                winningProposal_ = p;
            }
        }
    }
    function winnerName() public view
            returns (bytes32 winnerName_)
    {// 根据提案数组中的获胜者的索引，返回获胜者的name名称
        winnerName_ = proposals[winningProposal()].name;
    }
}
```

明天继续在本文中 更新 silidity 详细语法操作

---

## 2021-03-05 更新内容

## 基础操作整理

1.solidity 四种访问权限

```javasctipt
public //任何人都可以调用该函数，包括DApp的使用者。
private //只有合约本身可以调用该函数 在另外一个函数中。
internal //只有这份合同以及由此产生的所有合同才能称之为合同。
external //只有外部函数可以调用。内部无法调用。
```

2.  solidity 三种修饰符

```javascript
view; //可以自由调用，无需重复花费gai，因为它只是查看区块状态，而不是改变它。
pure; //可以自由调用，既不读取，也不写入。
payable; //经常用做将代币发送给合约地址。
```

3.  一个 solidity 的函数应有以下部分组成

```javascript
function // 函数名字
访问权限
修饰符
returns

pragma solidity ^0.4.0;
contract helloworld{
    function string(string inputstr) public view returns(string){
        return inputstr
    }
}
```

4.  solidity 布尔类型

```javascript
pragma solidity ^0.4.0;
contract helloworld{
    bool boola=true;

    function booltesta() public view returns(bool){
        return bools
    }
    function booltestb(int a,int b) public view  returns(bool){
        return a==b
    }
}
```

5.  与或运算 && ||

```javascript
pragma solidity ^0.4.0;
constract helloworld{
    function test1() public view returns(bool){
        return true&&true
    }
    function test2() public view returns(bool){
        return true&&false;
    }
    function test3() public view returns(bool){
        return false&&false;
    }
    function test4() public view returns(bool){
        return true||true
    }
    function test5() public view returns(bool){
        return true||false
    }
}
```

6.  通常运算，加减乘除 幂运算 2\*\*3 代表 2 的 3 次幂

```javascript
pragma solidity ^0.4.0
contract helloworld {
    function add(int a,int b) public view returns(int){
        return a+b;
    }
    function subtr(int a, int b) public view returns(int){
        return a-b
    }
    function multi(int a,int b) public view returns(int){
        return a*b
    }
    function divi(int a,int b) public view returns(int){
        return a/b
    }
    function more(int a,int b) public view returns(int){
        return a%b
    }
    function mi(int a,int b) public view returns(int){
        return a**b
    }
}
```

7.  位运算。（操作数转换为二进制后每位进行运算）（重点！）

```
&：同1取1。
|：有1取1。
~：直接相反。
^：不同取1。
<<：向左移动 x 位的操作。
>>：向右移动 x 位的操作。
```

```javascript

pragma solidity ^0.4.0;
contract helloworld{
    uint8 a =3;
    uint8 b=4;
    function test1() public view returns (uint8){
        return a&b;  // 0
        //与运算，只有都是1 才会取1 ，否则取0
        // 0 0 0 0 0 0 0 1 1 :3
        // 0 0 0 0 0 0 1 0 0 :4

        // 0 0 0 0 0 0 0 0 0 :0
    }
    function test2() public view returns (uint8){
        return a|b;  // 7

        //或运算，只要有1 就取1 ，否则取0
        // 0 0 0 0 0 0 0 1 1 :3
        // 0 0 0 0 0 0 1 0 0 :4

        // 0 0 0 0 0 0 1 1 1 :7
    }
    function test3() public view returns (uint8){
        return ~a;  // 252

        //非运算，取反
        // 0 0 0 0 0 0 0 1 1 :3

        // 1 1 1 1 1 1 1 0 0 :252
    }
    function test4() public view returns (uint8){
        return a^b;  // 7

        //异或运算，只要不相同 就取1 ，否则取0
        // 0 0 0 0 0 0 0 1 1 :3
        // 0 0 0 0 0 0 1 0 0 :4

        // 0 0 0 0 0 0 1 1 1 :7
    }
    function test5() public view returns (uint8){
        return a<<1;  // 6

        //向左移动 1 位
        // 0 0 0 0 0 0 0 1 1 :3

        // 0 0 0 0 0 0 1 1 0 :6
    }
    function test6() public view returns (uint8){
        return a>>1;  // 1

        //向右移动 1 位
        // 0 0 0 0 0 0 0 1 1 :3

        // 0 0 0 0 0 0 0 0 1 :1
    }
}
```

8.  solidity 中的赋值。

```javascript
pragma solidity ^0.4.0;

contract helloworld {
    function setvaluetest() public view returns(uint){
        uint num=9999999999999999999999999999-9999999999999999999999999998;
        return num;
        //结果 1
        //solidity 中的赋值是先计算 再赋值。
    }
}
```

明天继续

---
