---
title: TradingView中文开发文档
categories: V1.0.1
tags: TradingView
date: 2022-08-18 11:00:00
---

### 按钮

您可以将自己的按钮添加到顶部工具栏。

[createButton(options)](Widget-Methods.md#createbuttonoptions)

### 样式

图表库具有 2 个预定义的颜色主题。 它们是显示图表的最佳选择。 但是，从 1.16 开始，您可以更改工具栏的颜色以使其完全适合您的背景。 您可以在[themes.html](https://github.com/tradingview/charting_library/blob/unstable/themed.html) 中看到自定义的示例。
