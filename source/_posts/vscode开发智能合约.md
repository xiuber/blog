---
title: 使用vscode编辑器，开发智能合约
categories: 区块链
tags: 区块链
date: 2021-03-03 11:00:00
---

## 简介

本文只针对使用 vscode 编辑器，对 solidity 智能合约编写环境的创建，Truffle 框架的使用，合约编译及本地模拟部署等操作。暂不考虑更深层次或者其他服务操作。（cos：我也不会啊 ）

## 环境简介

1.  vscode 安装 solidity 插件（或者 Truffle 详见 3），solidity 插件提供了简单编译功能。本文主要介绍利用 Truffle 框架 来快速对合约进行开发。

[![solidity](https://s3.ax1x.com/2021/03/03/6AY0kq.md.png)](https://imgtu.com/i/6AY0kq)

2.  npm 全局安装 solc 编译器,当然本地项目中安装也可，看心情吧。

```
npm install -g solc
```

3.  Truffle 安装，一款为了方便开发、调试、部署智能合约的框架。因为 truffle 会集成 web3.js 等，所以暂时不对 web3 进行赘述。

[![truffle version](https://s3.ax1x.com/2021/03/03/6EiJG6.png)](https://imgtu.com/i/6EiJG6)

```
npm install -g truffle
```

4.  [Ganache](https://www.trufflesuite.com/ganache) 可以启动一个个人的以太坊私有链用作测试、执行命令、检测区块执行状态。请注意，重要：truffle develop 命令 也可以启动一个虚拟私有链，每次自动创建 10 个账户，类似 Ganache 工具。本文我们使用 Ganache 工具 来启动一个虚拟链，比较直观。

5.  扩展：SCStiudio 安装，清华大学贡献的合约安全分析工具 SCStudio，在合约的开发过程中使用 SCStudio 进行实时安全性检查，从而在开发阶段就将安全漏洞拒之门外。

[![SCStiudio](https://s3.ax1x.com/2021/03/03/6AtCjg.md.png)](https://imgtu.com/i/6AtCjg)

## vscode solidity 插件

环境装好后，我们先来说说 vs solidity 插件如何使用。新建一个文件夹，vscode 打开，在其中创建一个简单的合约，例如：

[![solidity](https://s3.ax1x.com/2021/03/03/6AwLDS.md.png)](https://imgtu.com/i/6AwLDS)

右下角，SCStudio Ready 则是我们安装的扩展插件：SCStiudio，可以实时进行合约的规范和安全检查。如果出现错误，则会标红提示。

[![error](https://s3.ax1x.com/2021/03/03/6A0ugx.md.png)](https://imgtu.com/i/6A0ugx)

编译方法：直接 F5。编译成功后，会提示 Compilation completed successfully!

[![合约编译](https://s3.ax1x.com/2021/03/03/6A0cPs.md.png)](https://imgtu.com/i/6A0cPs)

同时会生成一个 bin 文件，包含合约编译后的 bin，abi 等相关信息。

[![bin](https://s3.ax1x.com/2021/03/03/6ABXfs.md.png)](https://imgtu.com/i/6ABXfs)

## Truffle

1.  初始化一个项目。

```
truffle init
```

也可以使用 truffle unbox box 名称 在 [truffle box 仓库](https://www.trufflesuite.com/boxes)下载个 demo，例如：truffle unbox pet-shop

init 完成后，我们可以看到如下目录：

[![truffle init](https://s3.ax1x.com/2021/03/03/6ArsaD.md.png)](https://imgtu.com/i/6ArsaD)

```
|- contracts /智能合约的文件夹，所有的智能合约文件都放置在这里。里面包含一个重要的合约Migrations.sol
|- migrations /用来处理部署（迁移）智能合约 ，迁移是一个额外特别的合约用来保存合约的变化。
|-test /智能合约测试用例文件夹
|-truffle-config.js /配置文件
```

我们使用如下命令来创建一个 SimpleStorage 合约:

```
truffle create contract SimpleStorage
```

之后我们看到 contracts 文件下 自动生成了一个 SimpleStorage.sol 的初始化合约。接下来我们把它完善。

[![truffle create](https://s3.ax1x.com/2021/03/03/6AyAhQ.md.png)](https://imgtu.com/i/6AyAhQ)

之后我们启动 ganache，打开一个个人本地的节点服务，默认地址为 http:// 127.0.0.1:7545，networkid：5777
（注意：ganache 软件只是打开的一个本地节点服务，供我们进行合约的开发测试，关闭后 所有节点数据将不再存在。）

[![ganache](https://s3.ax1x.com/2021/03/03/6AWD4f.md.png)](https://imgtu.com/i/6AWD4f)

我们打开 truffle-config.js 配置文件，配置一下 network

[![truffle-config](https://s4.ax1x.com/2021/03/03/6A4db9.png)](https://imgtu.com/i/6A4db9)

使用 truffle compile 命令编译合约,

```
truffle compile
```

编译后我们可以看到生成了一个 build 文件，里面是我们合约的 json，记录了 bin，abi 相关信息。

[![compile](https://s4.ax1x.com/2021/03/03/6A5gJ0.md.png)](https://imgtu.com/i/6A5gJ0)

我们也可以使用 truffle console --network ganache 进入 truffle 控制台，直接使用 compile 命令来编译和部署，同 truffle compile：如图：（本文均还使用 truffle 命令，这里只是简单介绍下控制台的使用）

[![6AIakR.png](https://s4.ax1x.com/2021/03/03/6AIakR.png)](https://imgtu.com/i/6AIakR)

编译过后，因为我们已经通过 ganache 软件 启动配置了一个节点服务，所以可以直接使用命令来部署合约。

```
truffle  migrate
```

[![truffle  migrate](https://s4.ax1x.com/2021/03/03/6ATtiR.md.png)](https://imgtu.com/i/6ATtiR)

这时候 我们看到 Migrations 合约已经部署成功，但是我们创建的 SimpleStorage 合约并没有显示部署成功，是因为我们在 migrations 文件中 缺少 SimpleStorage.js 文件 用来部署。

[![SimpleStorage](https://s4.ax1x.com/2021/03/03/6A7E6K.md.png)](https://imgtu.com/i/6A7E6K)

执行命令创建 deploy_SimpleStorage 文件:

```
truffle create migration deploy_SimpleStorage
```

我们看到 migrations 目录已经生成该文件，内容和 Migrations 相似。

注意！重要：我们也可以选择在 1_initial_migration.js 文件中进行 deploy 配置：

```javascript
const Migrations = artifacts.require("Migrations");
const SimpleStorage = artifacts.require("SimpleStorage");
module.exports = function (deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(SimpleStorage);
};
```

[![生成部署文件](https://s4.ax1x.com/2021/03/03/6A7D10.md.png)](https://imgtu.com/i/6A7D10)

之后我们重新部署合约

```
truffle migrate --reset //部署合约，--reset表示强制重新部署
```

然后我们可以看到 两个合约均被部署成功

[![部署成功](https://s4.ax1x.com/2021/03/03/6A77nO.md.png)](https://imgtu.com/i/6A77nO)

从部署成功的信息我们可以看到，合约地址，hash，花费的 gas 等信息。部署合约也是需要支付一定的 eth 的，其实也是一种交易操作。我们打开 ganache 软件，发现我们的 eth 变少了。
[![ganache 软件](https://s4.ax1x.com/2021/03/03/6AHaDO.md.png)](https://imgtu.com/i/6AHaDO)
到这里，如何使用 vscode 进行合约编写，编译，部署就算完成。

具体的 Solidity 语言编写合约，以及 Solidity 语法，和 truffle 等（Solidity 智能合约语法,truffle 的更多操作与 利用 web3.js 和节点交互调用合约等），陆续更新。

不断学习中，必有瑕疵和值得改进之处，欢迎联系我 多多指教。
